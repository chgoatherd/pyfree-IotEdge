#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef DISK_SPACE_H
#define DISK_SPACE_H

/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : DiskSpace.h
  *File Mark       : 
  *Summary         : 
  *磁盘空间统计和日志删除相关函数集
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include <string>

namespace pyfree
{
  /**
	 * 根据指定磁盘获取磁盘的总空间和剩余空间
	 * @param _DiskStr {char} 磁盘目录,一般win指定{C,D},linux指定{/}
	 * @param _totalSize {int} 返回磁盘总空间
   * @param _freeSize {int} 返回磁盘剩余空间
	 * @return {int} 预留的返回值,暂指定是常量1
	 */
	int getDiskFreeSpace(char _DiskStr, int &_totalSize, int &_freeSize);
  /**
	 * 根据指定目录 扩展名 天数限制等删除目录下的文件,项目用于删除旧日志操作
	 * @param _dir {string}} 目录
	 * @param extname {string} 扩展名,如txt/ini/log等
   * @param dayForLimit_ {int} 指定天数，默认为0时会删除非当天的其他文件
	 * @return {void} 无返回
	 */
	void moveOldFile(std::string _dir, const std::string &extname, int dayForLimit_=0);
  /**
	 * 获取当天凌晨时刻的偏移时间,单位秒,与1970-01-01 00:00:00起
	 * @param deviation {int} 偏移秒数
	 * @return {int} 当天凌晨时刻的偏移时间.单位秒
	 */
	int getCurDayZeroClockTime(int deviation=0);
};

#endif
