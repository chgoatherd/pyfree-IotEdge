#ifndef _VERSION_H_
#define _VERSION_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : version.h
  *File Mark       : 
  *Summary         : 版本输出 license检测
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
namespace pyfree
{
    /**
     * license校验
     * @return {bool } 校验是否吻合
     */
    bool LicenseCheck();
    /**
     * 程序相关信息输出
     * @return {void }
     */
    void versionLog();
    /**
     * 程序入口运行参数校验
     * @param argc {int} 参数个数
     * @param argv {char**} 参数集
     * @return {void }
     */
    void checkArg(int argc, char* argv[]);
};

#endif // !_VERSION_H_