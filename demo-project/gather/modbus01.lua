﻿--应用脚本
--脚本语言:LUA (http://www.lua.org )
--项目应用:pyfree项目
--脚本版本:1.0
--需要配置总召函数（getTotalCallCMD），用户返回总召指令；
--配置下控函数（getDownControl）用于将信息点-值的数据对转换为下控指令；
--配置响应（返回脚本解析）函数（getReciveIDVAL），用于将返回指令转换为信息点-值的数据对。
-----------------------------------------------------------------------------------------------------------------------------------------
-- 总召指令
function getTotalCallCMD()
	-- allcall为返回指令,HEX格式
	local allcall = "01030000000A"
	--返回标识[0,不做任何改变],[1,加HEX+CRC] ,[2,加HEX->ASCII] ,[3,加(HEX +CRC)->ASCII]
	return allcall,1
end
--获取下控指令,参数:作业类型[查询/设值] 点类型[遥调/遥信] 设备地址 数据值
function getDownControl(exetype, ptype, addr, val)
	--collectgarbage("collect")
	-- controlCmd为返回指令,Hex格式
	local controlCmd = "NULL"
	-- check为返回标识 [1,加HEX+CRC] ,[2,加HEX->ASCII] ,[3,加(HEX +CRC)->ASCII]
	local check = 1
	
	if 1==exetype then
		controlCmd = "01030000000AC5CD"
		check = 0
	else
		if 1==ptype then
			controlCmd = "0106"..string.format("%04X",addr)..string.format("%04X",val)
		elseif 2==ptype then
			controlCmd = "0106"..string.format("%04X",addr)..string.format("%04X",val)
		else
			return controlCmd,check
		end
	end

	return controlCmd,check
end
--获取响应指令解析结果,参数:上行（返回）指令 下行指令
function getReciveIDVAL(up_cmd,down_cmd)
	--local c1 = collectgarbage("count");
	--print("start:",c1);
	local stra = up_cmd
	--print(stra)
	local recs = ""
	--print("...1...\n")
	if string.sub( stra, 1, 4 )=="0106" then
		local hid = string.sub( stra, 5, 6 )
		local lid = string.sub( stra, 7, 8 )
		local id = tonumber("0X"..hid)*256+tonumber("0X"..lid)
		
		local hval = string.sub( stra, 9, 10 )
		local lval = string.sub( stra, 11, 12 )
		local val = tonumber("0X"..hval)*256+tonumber("0X"..lval)
		
		recs = string.format("%d,%d",id,val)
		return recs,1
	end
	if string.sub( stra, 1, 4 )~="0103" then
		return recs,0
	end
	--print("...2...\n")
	if string.len(stra)<14 then
		return recs,0
	end
	--print("...3...\n")
	if 0~=(string.len(stra)%2) then
		return recs,0
	end
	--print("...4...\n")
	local lLen = string.sub( stra, 5, 6 )
	local dlen = tonumber("0x"..lLen)
	--print(dlen)
	if dlen<=0 then
		return recs,0
	end
	local size = 0
	--print("...5...\n")
	for i=1,2*dlen,4 do
		local hval = string.sub( stra, i+6, i+7 )
		local lval = string.sub( stra, i+8, i+9 )
		local val = tonumber("0X"..hval)*256+tonumber("0X"..lval)
		local valmap = string.format("%d,%d",size,val)
		--print(valmap)
		recs = recs..valmap
		size = size+1
		if i<2*dlen then
			recs = recs..";"
		end
	end
	--print("...6...\n")
	--local c2 = collectgarbage("count");
	--print("end:",c2);
	-- recs为返回字符串,采用”点编号,值;点编号,值”样式
	-- size为返回数据对（点编号,值）的个数
	return recs,size
	--return "1,2;5,6",2
end
