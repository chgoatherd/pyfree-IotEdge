#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _BUSINESS_DEF_H_
#define _BUSINESS_DEF_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : business_def.h
  *File Mark       : 
  *Summary         : 业务信息集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "conf_app.h"
#include "conf_channel.h"
#include "conf_pmap.h"
#include "datadef.h"
#include "hashmap.h"

class BusinessDef
{
public:
	static BusinessDef* getInstance();
	static void Destroy();
	~BusinessDef();
	//
	bool getPTO(int _gid, int _pid, pyfree::PType _ptype, pyfree::PTo &_pto);
	bool getPFrom(int _pid, pyfree::PType _ptype,pyfree::PFrom &pf);
	bool getPOS(std::map<int,LiveData> &tran_infos);
	//////////////////////////////////////////////////////////

	char getDiskSymbol();
	int getFreeSizeLimit();
	std::string getLogDir();
	void outAppConf();
	bool getNetInfoByGID(int gid_,pyfree::NetPort &item);
	bool getSerialInfoByGID(int gid_,pyfree::SerialPort &item);
	bool getProtocolInfoByGID(int gid_,pyfree::ProtocolDef &item);
	bool getGatherAttsByID(int gid_,pyfree::GatherAtts &atts_);
	bool getPoitInfoByID(int gid_,std::list<pyfree::PInfo> &list);
	bool getFPoitInfoByID(int gid_,std::list<pyfree::PFuncInfo> &list);
	//
	bool getNetInfoByTID(int tid_,pyfree::NetPort &item);
	bool getSerialInfoByTID(int tid_,pyfree::SerialPort &item);
	bool getTranInfoByID(int tid_,pyfree::Transmit &item);
	bool getProtocolInfoByTID(int tid_,pyfree::ProtocolDef &item);
	bool getProtocolTypeByTID(int tid_,int &ptype);
	//
	pyfree::ComManagerDef commdef;
private:
	BusinessDef() {
		init();
	};
	BusinessDef& operator=(const BusinessDef&) {return *this;};
	void init();
private:
	static BusinessDef* instance;
	pyfree::GatherConf appConf;
	std::list<pyfree::PMap> pmaps;

	MyObj_FT<pyfree::PTo> ftmap;
	MyObj_TF<pyfree::PFrom> tfmap;
};

#endif //_BUSINESS_DEF_H_
