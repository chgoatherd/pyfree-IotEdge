#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _CONF_READ_H_
#define _CONF_READ_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_read.h
  *File Mark       : 
  *Summary         : 业务配置信息加载函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <map>
#ifdef __linux__
#include <string>
#endif
#include "conf_app.h"
#include "conf_channel.h"
#include "conf_pmap.h"

namespace pyfree
{
  /**
	 * 读取程序相关配置信息
	 * @param conf {GatherConf} 程序配置信息的结构化数据
   * @param xml_ {string} 配置文件名
	 * @return {void } 
	 */
	void readAppConf(GatherConf &conf, std::string xml_ = "appconf.xml");
  /**
	 * 读取采集/转发信道的相关配置
	 * @param commdef {ComManagerDef} 信道配置集
   * @param xml_ {string} 配置文件名
	 * @return {void } 
	 */
	void readGathers(ComManagerDef &commdef, std::string xml_ = "gather.xml");
  /**
	 * 读取采集信息点与转发信息点的映射集
	 * @param pmaps {list} 点映射集
   * @param xml_ {string} 配置文件名
	 * @return {void } 
	 */
	void readPMaps(std::list<PMap> &pmaps, std::string xml_ = "gmap.xml");
};
#endif //CONF_READ_H