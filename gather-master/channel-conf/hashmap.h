#pragma once
#ifndef HASH_MAP_H
#define HASH_MAP_H
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : hashmap.h
  *File Mark       : 
  *Summary         : 自定义map容器的Key
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "dtypedef.h"

#include <map>
#include <iostream> 

class KeyObj_FT
{
public:
	KeyObj_FT(int _gid, int _id, pyfree::PType _type);
	//
	static int cmp_Key(const KeyObj_FT &obj1, const KeyObj_FT &obj2);

	int m_gid;
	int	m_id;
	pyfree::PType m_type;
private:
};
inline bool operator==(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) < 0; }
////////////////////////////////////////////////////////////////////////
template <class T>
class MyObj_FT
{
public:
	MyObj_FT();
	void insert(KeyObj_FT key, T val);
	bool getVal(KeyObj_FT key, T &_val);
	int size();
	//其他函数类似实现
private:
	std::map<KeyObj_FT, T> data;
};
////////////////////////////////////////////////
template <class T>
MyObj_FT<T>::MyObj_FT()
{
};

template <class T>
void MyObj_FT<T>::insert(KeyObj_FT key, T val)
{
	data[key] = val;
};

template <class T>
bool MyObj_FT<T>::getVal(KeyObj_FT key, T &_val)
{
	typename std::map<KeyObj_FT,T>::iterator it = data.find(key);
	if (it != data.end()) {
		_val = it->second;
		return true;
	}
	return false;
};
template <class T>
int MyObj_FT<T>::size()
{
	return static_cast<int>(data.size());
};

//////////////////////////////////////////////////////////
class KeyObj_TF
{
public:
	KeyObj_TF(pyfree::PType _pType, int _pID);
	//
	static int cmp_Key(const KeyObj_TF &obj1, const KeyObj_TF &obj2);

	pyfree::PType	m_pType;
	int m_pID;
private:

};
inline bool operator==(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) < 0; }

template <class T>
class MyObj_TF
{
public:
	MyObj_TF();
	void insert(KeyObj_TF key, T val);
	bool getVal(KeyObj_TF key,T &_val);
	int size();
	//其他函数类似实现
private:
	std::map<KeyObj_TF, T> data;
};
////////////////////////////////////////////
template <class T>
MyObj_TF<T>::MyObj_TF()
{
};

template <class T>
void MyObj_TF<T>::insert(KeyObj_TF key, T val)
{
	data[key] = val;
};

template <class T>
bool MyObj_TF<T>::getVal(KeyObj_TF key,T &_val)
{
	typename std::map<KeyObj_TF, T>::iterator it = data.find(key);
	if (it != data.end()) {
		_val = it->second;
		return true;
	}
	return false;
};
template <class T>
int MyObj_TF<T>::size()
{
	return static_cast<int>(data.size());
};
//////////////////////////////////////
class KeyObj_GAddr
{
public:
	KeyObj_GAddr(unsigned long long m_d_addr_, int m_addr_);
	//
	static long cmp_Key(const KeyObj_GAddr &obj1, const KeyObj_GAddr &obj2);

	unsigned long long m_d_addr;
	int	m_addr;
	int linkFlag;
private:
};
inline bool operator==(const KeyObj_GAddr& obj1, const KeyObj_GAddr& obj2) { return KeyObj_GAddr::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_GAddr& obj1, const KeyObj_GAddr& obj2) { return KeyObj_GAddr::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_GAddr& obj1, const KeyObj_GAddr& obj2) { return KeyObj_GAddr::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_GAddr& obj1, const KeyObj_GAddr& obj2) { return KeyObj_GAddr::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_GAddr& obj1, const KeyObj_GAddr& obj2) { return KeyObj_GAddr::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_GAddr& obj1, const KeyObj_GAddr& obj2) { return KeyObj_GAddr::cmp_Key(obj1, obj2) < 0; }

//////////////////////////////////////
class KeyObj_GClient
{
public:
	KeyObj_GClient();
	KeyObj_GClient(unsigned long ip_, int port_);
	//
	static long cmp_Key(const KeyObj_GClient &obj1, const KeyObj_GClient &obj2);

	unsigned long m_ip;
	int	m_port;
	int linkFlag;
private:
};
inline bool operator==(const KeyObj_GClient& obj1, const KeyObj_GClient& obj2) { return KeyObj_GClient::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_GClient& obj1, const KeyObj_GClient& obj2) { return KeyObj_GClient::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_GClient& obj1, const KeyObj_GClient& obj2) { return KeyObj_GClient::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_GClient& obj1, const KeyObj_GClient& obj2) { return KeyObj_GClient::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_GClient& obj1, const KeyObj_GClient& obj2) { return KeyObj_GClient::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_GClient& obj1, const KeyObj_GClient& obj2) { return KeyObj_GClient::cmp_Key(obj1, obj2) < 0; }

//////////////////////////////////////
class KeyObj_Addr
{
public:
	KeyObj_Addr(int _addr, pyfree::PType _type);
	//
	static int cmp_Key(const KeyObj_Addr &obj1, const KeyObj_Addr &obj2);

	int	m_addr;
	int	m_ptype;
private:
};
inline bool operator==(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) < 0; }

#endif //HASH_MAP_H

