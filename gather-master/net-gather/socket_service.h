#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_SERVICE_H_
#define _SOCKET_SERVICE_H_

#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

class SocketPrivate_ACL;

class SocketService : public acl::thread
{
public:
	SocketService();
	~SocketService();

	void setPDataPtr(SocketPrivate_ACL *socket_acl_);
	void* run();
private:
	SocketPrivate_ACL *socket_acl;
};
#endif
