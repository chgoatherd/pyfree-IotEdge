#ifndef _CONF_P_MAP_H_
#define _CONF_P_MAP_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_pmap.h
  *File Mark       : 
  *Summary         : 业务数据读取函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>
#include "dtypedef.h"

namespace pyfree
{
//映射配置
struct PFrom
{
	std::string		ipStr;
	unsigned long long	ipLong;
	unsigned int	pID;
	PType			pType;
	//来自映射方的描述信息,效率冗余信息,2018-11-22增加,为了短信可采用描述字段
	std::string		devDesc;
	std::string		pDesc;
	EventWay		eway;
};

struct PTo
{
	long long		devID;
	DevType			devType;
	unsigned int	pID;
	PType			pType;
	//描述信息,来自dev.xml配置
	std::string		devDesc;
	std::string		pDesc;
	EventWay		eway;
};

struct PMap
{
	PFrom from;
	PTo to;
};    
}
#endif 
