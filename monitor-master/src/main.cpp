#include <iostream>
#include <memory>
#include "lib_acl.h"

#include "version.h"
#include "Log.h"
#include "appexitio.h"

#include "business_def.h"
#include "socket_gather.h"
#include "socket_local.h"
#include "delay_plan_thread.h"
#include "plan_thread.h"
#include "alarm_thread.h"
#include "iot_to_aliyun.h"
#include "io_mqtt.h"
#include "timeUpInfo.h"
#include "verifyforcontrol.h"
#include "spaceMgr.h"
#include "udp_io_client.h"
#include "udp_wp_client.h"

namespace GlobalVar {
	bool exitFlag = false;
	extern std::string logname;
};
#ifdef WIN32
//server conf
char SVCNAME[128] = "pyfreeMonitor";
char SVCDESC[256] =
"\r\n pyfree Technology Ltd \r\n "
"https://gitee.com/pyzxjfree/pyfree-IotEdge \r\n "
"email:py8105@163.com \r\n "
"pyfree-monitor system service \r\n "
"Service installation success";
#endif // WIN32

#ifdef WIN32
int MyMain(int argc, char* argv[])
#else
int main(int argc, char *argv[])
#endif
{
	#ifdef WIN32
	GlobalVar::logname = std::string(SVCNAME);
	#else
	GlobalVar::logname = "pyfreeMonitor";	//最好与安装脚本保持一致,采用服务名
	#endif
	if (!pyfree::LicenseCheck())
	 {
		printf("license is error, please make sure software instance is right first!");
		exit(true);
	}
	pyfree::versionLog();
	pyfree::checkArg(argc,argv);
		//后续需要添加命令设置参数功能,例如PCS_Server.exe --setGatherPort=60002
	std::cout << "app cache data init start!" << std::endl;
	BusinessDef *bdef_ptr = BusinessDef::getInstance();
	CLogger::createInstance()->Log(MsgInfo,"cache data instance is init first!");
	printf("area:%s\n",bdef_ptr->getAreaInfo().areaDesc.c_str());
	//日志记录删除
	std::auto_ptr<DiskSpaceMgr> DiskSpaceMgr_ptr(NULL);
	DiskSpaceMgr_ptr.reset(new DiskSpaceMgr(bdef_ptr->getDiskSymbol(),bdef_ptr->getFreeSizeLimit()
		,bdef_ptr->getGLogDir(),"log"));
	DiskSpaceMgr_ptr->start();
	//采控软件通信
	std::cout << "gdata conf init start!" << std::endl;
	std::auto_ptr<SocketGather> ptr_gather(new SocketGather(bdef_ptr->getGatherIp(),bdef_ptr->getGatherPort()));
	CLogger::createInstance()->Log(MsgInfo,"gdata interface is init!");
	
	//记录
	std::cout << "record func init start!" << std::endl;
	std::auto_ptr<UdpIOClient> udp_io_record_ptr(NULL);
	if (bdef_ptr->getRecord_Func()) {
		udp_io_record_ptr.reset(new UdpIOClient(bdef_ptr->getRecord_PeerIp(),bdef_ptr->getRecord_PeerPort()
			,bdef_ptr->getRecord_LocalIp(),bdef_ptr->getRecord_LocalPort()));
		udp_io_record_ptr->start();
		CLogger::createInstance()->Log(MsgInfo,"record thread is init and start!");
	}else{
		CLogger::createInstance()->Log(MsgInfo, "record thread is off!");
	}
	//本地SOCKET客户端
	std::cout << "local socket init start!" << std::endl;
	std::auto_ptr<SocketLocal> socket_loacl_ptr(NULL);
	if (bdef_ptr->getLocalSocketFunc()) {
		socket_loacl_ptr.reset(new SocketLocal(bdef_ptr->getLocalSocketIp(),bdef_ptr->getLocalSocketPort()));
		CLogger::createInstance()->Log(MsgInfo, "socket interface for local client is init!");
	}
	else
	{
		CLogger::createInstance()->Log(MsgInfo, "socket interface for local client is off!");
	}
	//
	//任务计划
	std::cout << "plan init start!" << std::endl;
	std::auto_ptr<PlanThread> plan_ptr(NULL);
	std::auto_ptr<DelayPlanThread> plan_delay_ptr(NULL);
	if (bdef_ptr->getPlanFunc()) {
		plan_delay_ptr.reset(new DelayPlanThread());
		plan_delay_ptr->start();
		plan_ptr.reset(new PlanThread());
		plan_ptr->setDelayPlanThread(plan_delay_ptr.get());
		plan_ptr->start();
		CLogger::createInstance()->Log(MsgInfo, "taskplan thread is init and start!");
	}
	else
	{
		CLogger::createInstance()->Log(MsgInfo, "taskplan thread is off!");
	}
	//配置策略告警
	std::cout << "alarm init start!" << std::endl;
	std::auto_ptr<AlarmThread> alarm_ptr(NULL);
	if (bdef_ptr->getAlarmFunc()) {
		alarm_ptr.reset(new AlarmThread());
		alarm_ptr->start();
		CLogger::createInstance()->Log(MsgInfo, "alarm routting thread is init and start!");
	}
	else
	{
		CLogger::createInstance()->Log(MsgInfo, "alarm routting  thread is off!");
	}
	//事件告警,来自配置策略巡检 信息点自检 下控校验三方的告警事件,通过udp通信推送到告警推送软件(服务)
	std::cout << "waring init start!" << std::endl;
	std::auto_ptr<UdpWPClient> waring_ptr(NULL);
	if (bdef_ptr->getWP_Func()) {
		waring_ptr.reset(new UdpWPClient(bdef_ptr->getWP_PeerIp(),bdef_ptr->getWP_PeerPort()
			,bdef_ptr->getWP_LocalIp(),bdef_ptr->getWP_LocalPort()));
		waring_ptr->start();
		CLogger::createInstance()->Log(MsgInfo, "waring event thread is init!");
	}
	else
	{
		CLogger::createInstance()->Log(MsgInfo, "waring event thread is off!");
	}

	//阿里云物联网平台mqtt接口
	std::auto_ptr<IOToAliyun> ptr_IOMQTTAliyun(NULL);
	if (bdef_ptr->getAliyunIOFunc())
	{
		ptr_IOMQTTAliyun.reset(new IOToAliyun());
		ptr_IOMQTTAliyun->start();
		CLogger::createInstance()->Log(MsgInfo, "IOToAliyun is init!");
	}
	else {
		CLogger::createInstance()->Log(MsgInfo, "IOToAliyun is off!");
	}

	//通用mqtt接口,如RabbitMQ消息队列服务
	std::auto_ptr<IOMQTT> ptr_IOMQTT(NULL);
	if (bdef_ptr->getMqttFunc())
	{
		ptr_IOMQTT.reset(new IOMQTT());
		ptr_IOMQTT->start();
		CLogger::createInstance()->Log(MsgInfo, "IOMQTT is init!");
	}else {
		CLogger::createInstance()->Log(MsgInfo, "IOMQTT is off!");
	}
	//
	//信息点巡检,虚拟点数据定期上送;数据长期不变更和刷新告警
	std::cout << "TimeUpPInfo init start!" << std::endl;
	std::auto_ptr<TimeUpPInfo> ptr_TimeUpPInfo(NULL);
	if (bdef_ptr->getTimeUpFunc()|| bdef_ptr->getWaringEventFunc())
	{
		ptr_TimeUpPInfo.reset(new TimeUpPInfo());
		ptr_TimeUpPInfo->start();
		CLogger::createInstance()->Log(MsgInfo, "TimeUpPInfo thread is init and start!");
	}
	else
	{
		CLogger::createInstance()->Log(MsgInfo, "TimeUpPInfo thread is off!");
	}
	//
	//下控结果检验
	std::cout << "verification init start!" << std::endl;
	std::auto_ptr<VerifyForControl> ptr_vcc(NULL);
	if (bdef_ptr->getVerificationFunc()) {
		ptr_vcc.reset(new VerifyForControl());
		ptr_vcc->start();
		CLogger::createInstance()->Log(MsgInfo, "verification for control thread is init!");
	}
	else
	{
		CLogger::createInstance()->Log(MsgInfo, "verification for control thread is off!");
	}
	//
#ifdef WIN32
	if (SetConsoleCtrlHandler((PHANDLER_ROUTINE)ctrlhandler, true))
	{
		CLogger::createInstance()->Log(MsgInfo, "install signal handler success!");
	}
	else
	{
		CLogger::createInstance()->Log(MsgInfo, "install signal handler error!");
	}
#else
	SignalHandler * g_exit_handler = NULL;
	g_exit_handler = new SignalHandler();
	g_exit_handler->printf_out();
#endif // WIN32
	while(!GlobalVar::exitFlag){
		sleep(10);
	}
	return 0;
}
