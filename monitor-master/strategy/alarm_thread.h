#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _ALARM_THREAD_H_
#define _ALARM_THREAD_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : alarm_thread.h
  *File Mark       : 
  *Summary         : 
  *告警巡检线程,根据设定策略进行业务异常，类似于任务计划线程,但其执行指令中没有等待,延时等策略要求
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include <vector>

#include "conf_alarm.h"
#include "strategy.h"
#include "conf_war.h"
#include "queuedata_single.h"

// class EventCache;

class AlarmThread : public acl::thread, public Strategy
{
public:
	AlarmThread(void);
	~AlarmThread(void);
	void* run();
protected:

private:
	void doCmd(std::vector<AlarmCMD> planCmds,int planID);	//执行告警
private:
	bool running;
	QueueDataSingle<EventForWaring> *queueforwar;//告警事件缓存队列

	unsigned int timePlan;
	std::vector<pyfree::Alarm> plans;
};
#endif //_ALARM_THREAD_H_
