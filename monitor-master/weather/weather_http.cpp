#include "weather_http.h"

#ifdef WIN32
#define usleep(x) Sleep(x)
#endif
// #include "acl_cpp/lib_acl.hpp"  
#include "conf_app.h"
#include "business_def.h"
#include "strchange.h"
#include "pfunc.h"
#include "Log.h"

WeatherHttp::WeatherHttp()
	: running(true)
	, timeForData(0)
{
	BusinessDef *ptr_bdef = BusinessDef::getInstance();
	//天气预报采集,目前采用聚合数据公司的新闻接口
	pyfree::HttpWeather hweather = ptr_bdef->getHttpWeather();
	myArgHttp.startTime = hweather.startHourWeather;
	myArgHttp.endTime = hweather.endHourWeather;
	myArgHttp.isThreeHourCity = hweather.threeHourCityWeather;
	myArgHttp.addr_ = hweather.weatherhost;
	myArgHttp.path_ = hweather.weatherpath;
	myArgHttp.port_ = hweather.weatherport;
	myArgHttp.city_ = hweather.weathercity;
	myArgHttp.authorization = hweather.weatherauthorization;
	myArgHttp.upTime = hweather.weatherUpTime;
}

WeatherHttp::~WeatherHttp(void)
{
	running = false;
}

void* WeatherHttp::run()
{
	unsigned int curTime_ = static_cast<unsigned int>(time(NULL));
	timeForData = curTime_ + 10;
	while (running)
	{
		curTime_ = static_cast<unsigned int>(time(NULL));
		if (timeForData<curTime_)
		{
			if (mapCurTime()) 
			{
				timeForData = curTime_ + 60*myArgHttp.upTime;
				//code
				getHttpInfo();
			}
		}
		usleep(100);
	}

	return 0;
}

bool WeatherHttp::mapCurTime()
{
	return pyfree::isMapTime(60*myArgHttp.startTime,60*myArgHttp.endTime);
}

void WeatherHttp::getHttpInfo()
{
	bool accept_gzip = false;
	// bool send_body = false;

	//acl::log::stdout_open(true);
	acl::string aclurl, acladdr/*,aclContentType*/;
	aclurl.format("http://%s%s", myArgHttp.addr_.c_str(), myArgHttp.path_.c_str());
	acladdr.format("%s:%d", myArgHttp.addr_.c_str(), myArgHttp.port_);
	//aclContentType.format("application/json; charset=%s", myArgHttp.charset_.c_str());

	acl::http_request req(acladdr);

	acl::http_header& header = req.request_header();
	//
	//header.set_method(acl::HTTP_METHOD_POST);
	header.set_method(acl::HTTP_METHOD_GET);
	//
	//header.set_keep_alive(true);
	header.set_keep_alive(false);

	header.accept_gzip(accept_gzip ? true : false);

	//url
	header.set_url(aclurl);
	//host
	if (header.get_host() == NULL)
	{
		header.set_host(myArgHttp.addr_.c_str());
		Print_NOTICE(">>>set host: %s\r\n", myArgHttp.addr_.c_str());
	}
	else
	{
		Print_NOTICE(">>>host: %s\r\n", header.get_host());
	}
	
	//content_type
	//header.set_content_type(aclContentType);
	//header.set_content_length(1024);
	//param
	header.add_param("dtype", myArgHttp.dtype_.c_str());
	if (!myArgHttp.isThreeHourCity)
	{
		header.add_int("format", myArgHttp.format_);
	}
	header.add_param("cityname", ASCII2UTF_8(myArgHttp.city_).c_str());
	//entry
	header.add_entry("Authorization", myArgHttp.authorization.c_str());
	//cookie
	//header.add_cookie("x-cookie-name", "cookie-value");

	bool rc = req.request(NULL, 0);
	// 只所以将 build_request 放在 req.request 后面，是因为
	// req.request 内部可能会修改请求头中的字段
	acl::string hdr;
	header.build_request(hdr);
	CLogger::createInstance()->Log(MsgInfo, "request header:\r\n%s\r\n", hdr.c_str());

	if (rc == false)
	{
		CLogger::createInstance()->Log(MsgInfo, "send request error");
		//break;
		return;
	}

	Print_NOTICE("send request ok!\r\n");

	// 取出 HTTP 响应头的 Content-Type 字段  

	const char* p = req.header_value("Content-Type");
	if (p == NULL || *p == 0)
	{
		CLogger::createInstance()->Log(MsgInfo, "no Content-Type");
		return;
	}
	// 分析 HTTP 响应头的数据类型  
	acl::http_ctype content_type;
	content_type.parse(p);
	//p = NULL;
	// 响应头数据类型的子类型  
	const char* stype = content_type.get_stype();

	bool ret;
	if (stype == NULL)
	{
		ret = do_plain(req);
	}
//#ifdef WIN32
	//else if (_stricmp(stype, "xml") == 0)
//#else
	//else if (strcasecmp(stype, "xml") == 0)
//#endif
	//	ret = do_xml(req);
#ifdef WIN32
	else if (_stricmp(stype, "json") == 0)//linux->strcasecmp 
#else
	else if (strcasecmp(stype, "json") == 0)//linux->strcasecmp 
#endif
	{
		ret = do_json(req);
	}
	else
	{
		ret = do_plain(req);
	}
	if (ret == true)
	{
		CLogger::createInstance()->Log(MsgInfo, "read ok!");
	}
	//stype = NULL;
};

// 处理 text/plain 类型数据  
bool WeatherHttp::do_plain(acl::http_request& req)
{
	acl::string body;
	if (req.get_body(body/*, to_charset_.c_str()*/) == false)
	{
		CLogger::createInstance()->Log(MsgInfo, "get http body error");
		return false;
	}
	Print_NOTICE("body:\r\n(%s)\r\n", body.c_str());
	return true;
}

//// 处理 text/xml 类型数据  
//bool WeatherHttp::do_xml(acl::http_request& req)
//{
//	acl::xml body;
	//if (req.get_body(body/*, to_charset_.c_str()*/) == false)
//	{
		//CLogger::createInstance()->Log(MsgInfo, "get http body error");
//		return false;
//	}
//	acl::xml_node* node = body.first_node();
//	while (node)
//	{
//		const char* tag = node->tag_name();
//		const char* name = node->attr_value("name");
//		const char* pass = node->attr_value("pass");
//		printf(">>tag: %s, name: %s, pass: %s\r\n",
//			tag ? tag : "null",
//			name ? name : "null",
//			pass ? pass : "null");
//		node = body.next_node();
//	}
//	return true;
//}

void WeatherHttp::getCurTVHour(int &hour_, std::string &date_)
{
	time_t _t = time(NULL);
	// int min_ = 0;
	struct tm _tt;
#ifdef WIN32
	localtime_s(&_tt, &_t);
#else
	localtime_r(&_t, &_tt);
#endif
	hour_ = _tt.tm_hour;
	_tt.tm_year += 1900;
	_tt.tm_mon += 1;
	char buf[64] = { 0 };
	sprintf(buf, "%04d%02d%02d",_tt.tm_year,_tt.tm_mon,_tt.tm_mday);
	date_ = std::string(buf);
};

// 处理 text/json 类型数据  
bool WeatherHttp::do_json(acl::http_request& req)
{
	acl::json body;
	if (req.get_body(body/*, to_charset_.c_str()*/) == false)
	{
		CLogger::createInstance()->Log(MsgInfo, "get http body error");
		return false;
	}
	WeatherData wd;
	bool weatherIsRight = false;
	int hour_ = 0;
	std::string date_ = "";
	getCurTVHour(hour_, date_);
	acl::json_node* node = body.first_node();
	while (node)
	{
		if (node->tag_name())
		{
			std::string tagStr = std::string(node->tag_name());
			std::string valStr = std::string(node->get_text());
			if ("city" == tagStr) 
			{
				weatherIsRight = true;
				wd.city = UTF_82ASCII(valStr);
			}
			if ("date_y" == tagStr || "date" == tagStr)
			{
				wd.date = UTF_82ASCII(valStr);
			}
			if ("time" == tagStr) 
			{
				wd.time_ = UTF_82ASCII(valStr);
			}
			if ("week" == tagStr) 
			{
				wd.week = UTF_82ASCII(valStr);
			}
			if ("temperature" == tagStr)
			{
				wd.temperature = UTF_82ASCII(valStr);
			}
			if ("temp1" == tagStr)
			{
				wd.temperature_l = UTF_82ASCII(valStr);
			}
			if ("temp2" == tagStr)
			{
				wd.temperature_h = UTF_82ASCII(valStr);
			}
			if ("weather" == tagStr) 
			{
				wd.weather = UTF_82ASCII(valStr);
			}
			if ("weatherid" == tagStr || "fa" == tagStr)
			{
				wd.weather_ID_FA = UTF_82ASCII(valStr);
			}
			if ("fb" == tagStr) 
			{
				wd.weather_ID_FB = UTF_82ASCII(valStr);
			}
			if ("wind" == tagStr) 
			{
				wd.wind = UTF_82ASCII(valStr);
			}
			if ("uv_index" == tagStr) 
			{
				wd.uv_index = UTF_82ASCII(valStr);
			}
			if ("sh" == tagStr)
			{
				wd.sh = UTF_82ASCII(valStr);
			}
			if ("eh" == tagStr)
			{
				wd.eh = UTF_82ASCII(valStr);
			}
			printf("tag: %s", node->tag_name());
			if (!valStr.empty())
			{
				printf(", text: %s\r\n", UTF_82ASCII(valStr).c_str());
			}
			else {
				printf("\r\n");
			}
			if ("future" == tagStr) 
			{
				break;
			}
			if (!wd.sh.empty() && !wd.eh.empty() && !wd.date.empty())
			{
				if (date_ == wd.date)
				{
					int startHour = 0;
					int endHour = 0;
					if (!wd.sh.empty())
					{
						startHour = atoi(wd.sh.c_str());
					}
					if (!wd.eh.empty())
					{
						endHour = atoi(wd.eh.c_str());
					}
					if (startHour != endHour) {
						if (startHour > endHour)
						{
							endHour += 24;
						}
						if (startHour <= hour_ && endHour >= hour_)
						{
							weatherIsRight = true;
							wd.city = myArgHttp.city_;
							wd.temperature = wd.temperature_l + "℃~" + wd.temperature_h+"℃";
							break;
						}
					}
				}
				wd.sh = "";
				wd.eh = "";
				wd.date = "";
			}
		}
		node = body.next_node();
	}
	if (weatherIsRight) 
	{
		add(wd);
	}
	return true;
}

void WeatherHttp::add(WeatherData wd)
{
	m_MutexWeatherData.Lock();
	weatherData = wd;
	m_MutexWeatherData.Unlock();
}

bool WeatherHttp::getCurWeatherDesc(WeatherData &wd)
{
	bool ret = false;
	m_MutexWeatherData.Lock();
	if (!weatherData.weather.empty() && !weatherData.date.empty()) 
	{
		wd = weatherData;
		ret = true;
	}
	m_MutexWeatherData.Unlock();
	return ret;
}

bool WeatherHttp::getCurWeatherDesc(std::string &str)
{
	bool ret = false;
	m_MutexWeatherData.Lock();
	if (!weatherData.weather.empty() && !weatherData.date.empty())
	{
		char buf[512] = { 0 };
		if (!weatherData.city.empty())
		{
			sprintf(buf, "%s,%s,%s,%s,%s,%s,紫外强度:%s"
				, weatherData.city.c_str(), weatherData.date.c_str(), weatherData.week.c_str()
				, weatherData.temperature.c_str(), weatherData.weather.c_str()
				, weatherData.wind.c_str(), weatherData.uv_index.c_str());
		}
		else {
			sprintf(buf, "%s,%s,%s,%s"
				, myArgHttp.city_.c_str(), weatherData.date.c_str()
				, weatherData.temperature.c_str(), weatherData.weather.c_str());
		}
		str = std::string(buf);
		ret = true;
	}
	m_MutexWeatherData.Unlock();
	return ret;
}
