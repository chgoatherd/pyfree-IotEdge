cp -f libmosquitto.so.1 /usr/local/lib
cp -f libmosquittopp.so.1 /usr/local/lib
ln -sf /usr/local/lib/libmosquitto.so.1 /usr/local/lib/libmosquitto.so
ln -sf /usr/local/lib/libmosquittopp.so.1 /usr/local/lib/libmosquittopp.so
if [ ! -f "/etc/ld.so.conf.d/mosquitto_lib.conf" ];then
#添加程序动态库到系统
cat > /etc/ld.so.conf.d/mosquitto_lib.conf <<eof
/usr/local/lib
eof
#重新加载动态库，使其生效
ldconfig
fi
