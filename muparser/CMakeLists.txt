# CMake 最低版本号要求
cmake_minimum_required (VERSION 2.8)
# 项目信息
project (muparser)
#
if(WIN32)
    message(STATUS "windows compiling...")
    add_definitions(-D_PLATFORM_IS_WINDOWS_)
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
	set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")
else(WIN32)
    message(STATUS "linux compiling...")
    add_definitions( -D_PLATFORM_IS_LINUX_)
endif(WIN32)
#
set(LIBRARY_OUTPUT_PATH  ${PROJECT_SOURCE_DIR}/lib)
SET(muparser_h
    ${PROJECT_SOURCE_DIR}/include/muParser.h
    ${PROJECT_SOURCE_DIR}/include/muParserBase.h
    ${PROJECT_SOURCE_DIR}/include/muParserBytecode.h
    ${PROJECT_SOURCE_DIR}/include/muParserCallback.h
    ${PROJECT_SOURCE_DIR}/include/muParserDef.h
    ${PROJECT_SOURCE_DIR}/include/muParserDLL.h
    ${PROJECT_SOURCE_DIR}/include/muParserError.h
    ${PROJECT_SOURCE_DIR}/include/muParserFixes.h
    ${PROJECT_SOURCE_DIR}/include/muParserInt.h
    ${PROJECT_SOURCE_DIR}/include/muParserStack.h
    ${PROJECT_SOURCE_DIR}/include/muParserTemplateMagic.h
    ${PROJECT_SOURCE_DIR}/include/muParserTest.h
    ${PROJECT_SOURCE_DIR}/include/muParserToken.h
    ${PROJECT_SOURCE_DIR}/include/muParserTokenReader.h
)

SET(muparser_cpp
    ${PROJECT_SOURCE_DIR}/src/muParser.cpp
    ${PROJECT_SOURCE_DIR}/src/muParserBase.cpp
    ${PROJECT_SOURCE_DIR}/src/muParserBytecode.cpp
    ${PROJECT_SOURCE_DIR}/src/muParserCallback.cpp
    ${PROJECT_SOURCE_DIR}/src/muParserDLL.cpp
    ${PROJECT_SOURCE_DIR}/src/muParserError.cpp
    ${PROJECT_SOURCE_DIR}/src/muParserInt.cpp
    ${PROJECT_SOURCE_DIR}/src/muParserTest.cpp
    ${PROJECT_SOURCE_DIR}/src/muParserTokenReader.cpp
)
  
#头文件目录
include_directories(${PROJECT_SOURCE_DIR}/include())
#
# 指定生成目标
add_library(muParser STATIC ${muparser_h} ${muparser_cpp})
