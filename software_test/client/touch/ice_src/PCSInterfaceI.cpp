// **********************************************************************
//
// Copyright (c) 2003-2016 ZeroC, Inc. All rights reserved.
//
// **********************************************************************

#include <IceUtil/IceUtil.h>
#include <Ice/Ice.h>
#include "PCSInterfaceI.h"
#include "PCSClient.h"


using namespace std;

ClientAchieveI::ClientAchieveI(PCSClient* _client) : client(_client)
{
};

ClientAchieveI::~ClientAchieveI()
{

};

void ClientAchieveI::addDev(const ::PCS::Devs &devs, const ::Ice::Current&)
{
    if(client)
        client->addDev(devs);
};

void ClientAchieveI::addPInfo(::Ice::Long devID, const ::PCS::PInfos &pinfos, const ::Ice::Current&)
{
    if(client)
        client->addPInfo(devID,pinfos);
};

void ClientAchieveI::PValueChange(::Ice::Long devID
                              ,::Ice::Long pID
                              , const ::PCS::DateTimeI& itime
                              , ::Ice::Float val
                              , const ::Ice::Current&)
{
	if(client)
        client->PValueChange(devID,pID,itime,val);
};
