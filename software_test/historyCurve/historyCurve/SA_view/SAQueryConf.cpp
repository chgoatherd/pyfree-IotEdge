#include "SAQueryConf.h"

#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDateTimeEdit>
#include <QPushButton>
#include <QComboBox>
#include <QStringList>
#include <QDebug>

#include "plot/pfunc_qt.h"
#include "treemodel.h"
#include "SAFileTable.h"

SAQueryConfView::SAQueryConfView(QWidget * parent, Qt::WindowFlags f)
{
    QStringList cheaders;
    cheaders <<tr("file");
    TreeModel *cmodel= new TreeModel(cheaders);
    ptr_SAFileTableView = new SAFileTableView(cmodel,this);
    connect(ptr_SAFileTableView,SIGNAL(fileSelect(QString)),this,SIGNAL(fileSelect(QString)));

	myStartT = new QDateTimeEdit(this);
	myStartT->setDisplayFormat("yyyy-MM-dd hh:mm:ss.zzz");
	myEndT = new QDateTimeEdit(this);
	myEndT->setDisplayFormat("yyyy-MM-dd hh:mm:ss.zzz");

    QFormLayout *formLayout_01 = new QFormLayout();
    formLayout_01->setContentsMargins(0,0,0,0);
    formLayout_01->addRow(tr("startTime:"), myStartT);
    formLayout_01->addRow(tr("endTime:"), myEndT);

 	QPushButton *reload = new QPushButton(tr("ReloadSA"),this);
    connect(reload,SIGNAL(clicked()),this,SIGNAL(ReloadSA()));
 	QPushButton *drawButton = new QPushButton(tr("Draw"),this);
 	connect(drawButton,SIGNAL(clicked()),this,SLOT(DrawCurve()));

    curve_Type = new QComboBox(this);
    QStringList _typelist;
    _typelist << tr("Lines")
                << tr("Steps")
                << tr("Sticks");
    curve_Type->addItems(_typelist);
    connect(curve_Type,SIGNAL(currentIndexChanged(int)),this,SIGNAL(resetCurveType(int)));

    dev_combo = new QComboBox(this);
    QStringList _devlist;
    _devlist << "-1";
    dev_combo->addItems(_devlist);

    idx_combo = new QComboBox(this);
    QStringList _idlist;
    _idlist << "-1";
    idx_combo->addItems(_idlist);

 	QHBoxLayout *ptr_QHBoxLayout = new QHBoxLayout;
 	ptr_QHBoxLayout->setContentsMargins(0,0,0,0);
 	ptr_QHBoxLayout->addWidget(reload);
 	ptr_QHBoxLayout->addWidget(drawButton);
 	ptr_QHBoxLayout->addWidget(curve_Type);

    QHBoxLayout *ptr_QHBoxLayout01 = new QHBoxLayout;
    ptr_QHBoxLayout01->setContentsMargins(0,0,0,0);
    ptr_QHBoxLayout01->addWidget(dev_combo);
    ptr_QHBoxLayout01->addWidget(idx_combo);

 	QVBoxLayout *ptr_QVBoxLayout = new QVBoxLayout(this);
 	ptr_QVBoxLayout->setContentsMargins(0,0,0,0);
    ptr_QVBoxLayout->addWidget(ptr_SAFileTableView);
 	ptr_QVBoxLayout->addLayout(formLayout_01);
 	ptr_QVBoxLayout->addLayout(ptr_QHBoxLayout);
    ptr_QVBoxLayout->addLayout(ptr_QHBoxLayout01);
    this->setContentsMargins(0,0,0,0);
};

SAQueryConfView::~SAQueryConfView()
{
    if (NULL!=myStartT)
    {
        delete myStartT;
        myStartT = NULL;
    }
    if (NULL!=myEndT)
    {
        delete myEndT;
        myEndT = NULL;
    }
    if (NULL!=curve_Type)
    {
        delete curve_Type;
        curve_Type = NULL;
    }
};

void SAQueryConfView::setTimeRegion(long long _startT,long long _endT)
{
	QDateTime _startDT = PFUNC_QT::ChangeLong(_startT);
	// qDebug() << _startDT << "\n";
	myStartT->setDateTime(_startDT);
	myStartT->update();
	QDateTime _endDT = PFUNC_QT::ChangeLong(_endT);
	// qDebug() << _endDT << "\n";
	myEndT->setDateTime(_endDT);
	myEndT->update();
};

void SAQueryConfView::getTimeRegion(long long &_startT,long long &_endT)
{
	_startT = PFUNC_QT::ChangeDateTimeL(myStartT->dateTime());
	_endT = PFUNC_QT::ChangeDateTimeL(myEndT->dateTime());
//     qDebug()<<_startT<<":"<<_endT;
}

void SAQueryConfView::setDevIDList(QSet<int> _devids)
{
    QStringList _idlist;
    _idlist << "-1";
    foreach (const int &value, _devids)
        _idlist << QString("%1").arg(value);
    dev_combo->clear();
    dev_combo->addItems(_idlist);
}

void SAQueryConfView::setIDXList(QSet<int> _idxs)
{
    QStringList _idlist;
    _idlist << "-1";
    foreach (const int &value, _idxs)
        _idlist << QString("%1").arg(value);
    idx_combo->clear();
    idx_combo->addItems(_idlist);
}

void SAQueryConfView::DrawCurve()
{
    emit draw(dev_combo->currentText().toInt(),idx_combo->currentText().toInt());
};
