#include "SAThread.h"

#include <QDateTime>
#include <QDebug>
#include <QCoreApplication>

#include "src/confdeal.h"
#include "SA_db/SADB.h"
#include "plot/pfunc_qt.h"

SAThread::SAThread(QObject * parent)
	: QThread(parent)
    , runFlag(false)
	 , ptr_SADB(NULL)
     , sa_lasttime(0)
{
    ConfDeal *ptr_ConfDeal = ConfDeal::getInstance();
    drDir = ptr_ConfDeal->getdbDir();
	ptr_SADB = new SADB();
};

SAThread::~SAThread()
{
    try{
        runFlag = false;
        this->terminate();
        this->wait();
        if (ptr_SADB!=NULL)
        {
            if (ptr_SADB->IsOpen())
            {
                ptr_SADB->close();
            }
            delete ptr_SADB;
            ptr_SADB=NULL;
        }
        qDebug() << "SAThread::destroy \n";
    }catch(...){
        qDebug() << "SAThread::destroy fail\n";
    }
};

void SAThread::run()
{
	qDebug() << "SAThread::start \n";
	while(1)
	{
        // if (runFlag)
        // {
        //     if (sa_lasttime>1000&&sa_lordertime>=sa_lasttime)
        //     {
        //         //if user order more then agc table last time , read new data running
        //         QList<SADataItem> _data;
        //         SAQueryConf _qconf;
        //         _qconf.tvStart = sa_lasttime;
        //         if (ptr_SADB->getSADataQuery(_data,_qconf))
        //         {
        //             data_mutex.lock();
        //             for ( QList<SADataItem>::iterator it = _data.end();it!=_data.begin();)
        //             {
        //                 QList<SADataItem>::iterator it_ = (--it);
        //                 if (sa_lasttime<(*it_).Time)
        //                 {
        //                     sa_lasttime=(*it_).Time;
        //                     sa_lordertime = sa_lasttime;
        //                 }
        //                 if (_datas.size()>=saDSize)
        //                 {
        //                     _datas.removeLast();
        //                 }
        //                 _datas.prepend((*it_));
        //             }
        //             data_mutex.unlock();
        //             emit newItem(_data);
        //         }
        //     }
        // }
        this->msleep(1000);
	}
	exec();
}

bool SAThread::openDB(QString _path)
{
#ifdef WIN32
    QString div = "\\";
#else
    QString div = "/";
#endif
    QString _db = drDir+div+_path;
//    _db+="\\";
//    _db+=_path;
    // _db+=".db";
    if (ptr_SADB->IsOpen())
    {
        ptr_SADB->close();
    }
    if(!ptr_SADB->Open(_db.toLatin1().data()))
    {
        qDebug() << QString("cann't open db:%1\n").arg(_db);
        return false;
    }
    return true;
};

bool SAThread::loadDBConf(long long &_startT,long long &_endT)
{
    return getNewTimeRange(_startT,_endT);
};

bool SAThread::getNewTimeRange(long long &_startT,long long &_endT)
{
    if (!ptr_SADB->IsOpen())
        return false;
    sa_lasttime = 0;
    long long _lt = ptr_SADB->GetLastFT();
    // qDebug()<< "_lt="<<_lt;
    if (0==_lt)
    {
        qDebug() << QString("cann't get agc data last time\n");
        _endT = PFUNC_QT::ChangeDateTimeL(QDateTime::currentDateTime());
        _startT = _endT-3600000L;
        sa_lasttime = _endT;
        return false;
    }else{
        _endT = _lt;
        _startT = _lt-3600000L;
        sa_lasttime = _endT;
    }
    return true;
}

bool SAThread::loadDBData(long long _startT,long long _endT,QSet<int> &_devids,QSet<int> &_idxs)
{
    if (!ptr_SADB->IsOpen())
        return false;
    sa_lordertime = _endT;
    _datas.clear();
    SAQueryConf _qconf;
    _qconf.tvStart = _startT;
    _qconf.tvStop = _endT;
//     qDebug()<<"_qconf.tvStart="<<_qconf.tvStart<<","<<"_qconf.tvStop="<<_qconf.tvStop;
    if (!ptr_SADB->getSADataQuery(_datas,_devids,_idxs,_qconf))
    {
        qDebug() << QString("cann't get agc data info\n");
        return false;
    }
    return true;
};

bool SAThread::getDrawData(QMap<QString,QMap<QDateTime,qreal> > &_draws
    ,QMap<QString,LimitData> &val_limit,int _devid,int _idx)
{
    data_mutex.lock();
    for (QList<SADataItem>::iterator it = _datas.begin(); it!=_datas.end();it++)
    {
        if (((*it).Dev_Index==_devid||_devid<0) && ((*it).YC_Index==_idx||_idx<0))
        {
            QString _name = QString("[%1-%2]").arg((*it).Dev_Index).arg((*it).YC_Index);
            QMap<QString,QMap<QDateTime,qreal> >::iterator itf =  _draws.find(_name);
            if (itf == _draws.end())
            {
                _draws[_name] = QMap<QDateTime,qreal>();              
            }
            QMap<QString,LimitData>::iterator itl = val_limit.find(_name);
            if (itl == val_limit.end())
            {
                LimitData _ld;
                _ld.minValue = (*it).Value;
                _ld.maxValue = (*it).Value;
                _ld.min_dt_STR = (*it).dt_STR;
                _ld.max_dt_STR = (*it).dt_STR;
                val_limit[_name] = _ld;
            }else{
                if (itl.value().minValue>(*it).Value)
                {
                    val_limit[_name].minValue = (*it).Value;
                    val_limit[_name].min_dt_STR = (*it).dt_STR;
                }
                if (itl.value().maxValue < (*it).Value)
                {
                    val_limit[_name].maxValue = (*it).Value;
                    val_limit[_name].max_dt_STR = (*it).dt_STR;
                }
            }
            _draws[_name].insert((*it).dt_QDT,(*it).Value);
            
        }
    }
    data_mutex.unlock();
    for (QMap<QString,QMap<QDateTime,qreal> >::iterator it=_draws.begin();it!=_draws.end();it++)
    {
        if (!it.value().isEmpty())
        {
            return true;
        }
    }
    return false;
};


// bool SAThread::getSADataQuery(QList<SADataItem> &_datas)
// {
// 	if (!ptr_SADB->IsOpen())
// 		return false;
// 	return ptr_SADB->getSADataQuery(_datas);
// }

void SAThread::startRead()
{
    runFlag = true;
};

void SAThread::stopRead()
{
    runFlag = false;
};
