#ifndef SAView_H
#define SAView_H
/*
*agc数据展示窗口，划分为列表及曲线图展示AGC数据，结构树展示AGC配置三个区域
*/
#include <QWidget>

#include "SA_db/SADef.h"

QT_BEGIN_NAMESPACE
class AGCTreeView;
class SAQueryConfView;
class SATableView;
class PlotWidget;
class SAThread;
QT_END_NAMESPACE

class SAView : public QWidget
{
    Q_OBJECT
public:
    explicit SAView(QWidget *parent = 0, Qt::WindowFlags f = 0);
    ~SAView();
private:
    void init();
signals:
    void showMsg(QString _msg);
public slots:
	// void agcDetailCheck(QString _dv,QString _cz);
	void fileSelect(QString _name);
	void loadSAData();
	void getTimeZone();
	void resetCurveType(int _curveType);
	void itemDoubleClick(int _devid,int _idx);
	void addNewItem(QList<SADataItem> _data);
private:
	SAQueryConfView *ptr_SAQueryConfView;
	SATableView* ptr_SATableView;
	PlotWidget *ptr_PlotWidget;
	SAThread *ptr_SAThread;
	QString dbDir;
};

#endif //SAView_H