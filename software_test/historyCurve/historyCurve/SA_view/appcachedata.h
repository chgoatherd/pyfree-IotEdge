
#ifndef APPCACHEDATA_H
#define APPCACHEDATA_H

#include <QString>

class AppCacheData
{
public:
	static AppCacheData* getInstance();
	static void Destroy();
	~AppCacheData();

	QString getXmlNodes();
	void setXmlNodes(QString _nodes);

	QString getLineDiv();
	void setLineDiv(QString _div);
private:
	void init();

	AppCacheData();
	AppCacheData(const AppCacheData& ) {} // copy constructor
	AppCacheData& operator=(const AppCacheData& ) { return *this; } // assignment operator
private:
	static AppCacheData* instance;

	QString xmlnodes;
	QString linediv;
};

#endif