#include "importconf.h"

#include <QColor>
#include <QPalette>
#include <QFont>
#include <QApplication>
#include <QGridLayout>
#include <QPushButton>
#include <QStringList>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QFormLayout>
#include <QRegExp>
#include <QRegExpValidator>
#include <QMouseEvent>
#include <QFile>
#include <QMessageBox>
#include <QDebug>

#include "appcachedata.h"
#include "WinViewStyle/myfiledialog.h"

ImportConf::ImportConf(QStringList _list,QWidget * parent,int _fileDialogF,Qt::WindowFlags f)
	: QDialog(parent,f)
    , fileDialogF(_fileDialogF)
{
    win_init();
    data_init(_list);
}

ImportConf::~ImportConf()
{

}

void ImportConf::win_init()
{
    QFont _font("Courier New", 10);
    _font.setBold(true);
    // _font.setUnderline(true);
    this->setFont(_font);

    QPalette pl = this->palette();
    pl.setColor(QPalette::Base,QColor(240,240,240));
    pl.setColor(QPalette::Text,QColor(0,0,230));
    this->setPalette(pl);

    this->setWindowTitle(tr("importConf"));
    this->setWhatsThis(tr("user can set the div string with combobox and "
        "set a row which is more than 0 and a start row of the data will be imported."
        "user can set the map for the column of the import data list and the dispaly list all so."
        "warring, the column's index is start with 0."));
}

void ImportConf::data_init(QStringList _list)
{
    QGridLayout *_QGridLayout = new QGridLayout(this);

    QLabel *file_label = new QLabel(tr("filepath:"));
    file_label->setFixedWidth(fixedlabelW);
    _QGridLayout->addWidget(file_label,0,0,1,1);
    file_LineEdit = new QLineEdit();
    _QGridLayout->addWidget(file_LineEdit,0,1,1,2);
    filebrowse_Button = new QPushButton(tr("set"));
    filebrowse_Button->setFixedWidth(fixedButtonW);
    connect(filebrowse_Button,SIGNAL(clicked()),this,SLOT(getFilePath()));
    _QGridLayout->addWidget(filebrowse_Button,0,3,1,1);

    // QFormLayout *formLayout = new QFormLayout;
    AppCacheData *_AppCacheData = AppCacheData::getInstance();

    xml_label = new QLabel(tr("xmlNode:"));
    xml_label->setFixedWidth(fixedlabelW);
    _QGridLayout->addWidget(xml_label,1,0,1,1);
    xmlNode_LineEdit = new QLineEdit();
    xmlNode_LineEdit->setFixedWidth(3*fixedEditW);
    xmlNode_LineEdit->setText(_AppCacheData->getXmlNodes());
    _QGridLayout->addWidget(xmlNode_LineEdit,1,1,1,3);

    xml_label->hide();
    xmlNode_LineEdit->hide();

    QLabel *combo_label = new QLabel(tr("divCheck:"));
    combo_label->setFixedWidth(fixedlabelW);
    _QGridLayout->addWidget(combo_label,2,0,1,1);
    div_LineEdit = new QLineEdit();
    div_LineEdit->setFixedWidth(fixedEditW);
    div_LineEdit->setText(_AppCacheData->getLineDiv());
    _QGridLayout->addWidget(div_LineEdit,2,1,1,1);

    // formLayout->addRow(tr("divCheck"), div_QComboBox);

    QLabel *row_label = new QLabel(tr("startRow:"));
    row_label->setFixedWidth(fixedlabelW);
    _QGridLayout->addWidget(row_label,2,2,1,1);
    start_LineEdit = new QLineEdit();
    start_LineEdit->setFixedWidth(fixedEditW);
    QRegExp double_rx("^-?100|([0-9]{0,2})");
    start_LineEdit->setValidator(new QRegExpValidator(double_rx,this));
    start_LineEdit->setText("0");
    _QGridLayout->addWidget(start_LineEdit,2,3,1,1);

    // formLayout->addRow(tr("startRow"), start_LineEdit);

    _checklist.clear();

    for (int i=0; i<_list.size(); i++)
    {
        QLabel *map_label = new QLabel(_list.at(i)+":");
        map_label->setFixedWidth(fixedlabelW);
        _QGridLayout->addWidget(map_label,i/2+3,2*(i%2),1,1);
        QLineEdit* mapid = new QLineEdit();
        mapid->setFixedWidth(fixedEditW);
        // QRegExp double_rx("^-?100|([0-9]{0,2})");
        // mapid->setValidator(new QRegExpValidator(double_rx,this));
        mapid->setText(QString("%1,*").arg(i));
        _QGridLayout->addWidget(mapid,i/2+3,2*(i%2)+1,1,1);
        // formLayout->addRow(_list.at(i), mapid);
        _checklist.append(mapid);
    }

    // _QGridLayout->addLayout(formLayout,0,0,_list.size(),2);

    okButton = new QPushButton(tr("Ok"));
    okButton->setFixedWidth(fixedButtonW);
    cancelButton = new QPushButton(tr("Cancel"));
    cancelButton->setFixedWidth(fixedButtonW);
    connect(okButton,SIGNAL(clicked()),this,SLOT(setImportInfo()));
    connect(cancelButton,SIGNAL(clicked()),this,SLOT(close()));

    _QGridLayout->addWidget(okButton,_list.size()/2+3,2,1,1);
    _QGridLayout->addWidget(cancelButton,_list.size()/2+3,3,1,1);
    this->setLayout(_QGridLayout);
}

void ImportConf::setImportInfo()
{
    ImportConfInfo _inconf;
    _inconf.file = file_LineEdit->text();
    if(file_LineEdit->text().contains(".xml", Qt::CaseInsensitive))
    {
        _inconf.nodes = xmlNode_LineEdit->text().split(",",QString::SkipEmptyParts);
        if (_inconf.nodes.empty())
        {
            QMessageBox::warning(this, tr("xml nodes set"),
             tr("read xml, the xml node isn't set, please set it."));
            return;
        }
    }
    _inconf.div = div_LineEdit->text();
    _inconf.start = start_LineEdit->text().toInt();
    QMap<int,int> _list;
    for (int i = 0; i < _checklist.size(); i++)
    {
        QStringList _castList = _checklist.at(i)->text().split(",", QString::SkipEmptyParts);
        if(2!=_castList.size()){
            continue;
        }
        int _inId = _castList[0].toInt();
        if (_inId<0)
        {
            continue;
        }
        castInfo _ci;
        _ci.column = i;
        _ci.wildcard = _castList[1];
        qDebug() << _ci.wildcard <<" ";
        if(!_ci.wildcard.contains("*", Qt::CaseInsensitive)){
            _ci.wildcard ="*";
            qDebug()<<" .. ";
        }
        _ci.wildcard.replace("*","%1");
        qDebug() << _ci.wildcard << "\n";
        _inconf.list[_inId] = _ci;
    }
    AppCacheData *_AppCacheData = AppCacheData::getInstance();
    _AppCacheData->setXmlNodes(xmlNode_LineEdit->text());
    _AppCacheData->setLineDiv(div_LineEdit->text());
    emit sendConfInfo(_inconf);
    this->close();
}

void ImportConf::getFilePath()
{
    MyFileDialog *fileDialog = new MyFileDialog(this,fileDialogF);

    if(fileDialog->exec() == QDialog::Accepted) {
        file_LineEdit->setText(fileDialog->selectedFiles()[0]);
    }
    QFile file(file_LineEdit->text());
    if (!file.exists())
    {
        QMessageBox::warning(this, tr("Open Dialog"),
             tr("file %1 is not exist, please check it.")
             .arg(file.fileName()));
    }else{
        if(file_LineEdit->text().contains(".xml", Qt::CaseInsensitive))
        {
            xml_label->show();
            xmlNode_LineEdit->show();
        }else{
            xml_label->hide();
            xmlNode_LineEdit->hide();
        }
    }
}
