#include "myfiledialog.h"
#include <QCoreApplication>
// #include <QDebug>

MyFileDialog::MyFileDialog(QWidget * parent
		, int _model
		, const QString & caption
		, const QString & directory
		, const QString & filter )
	: QFileDialog(parent,caption,directory,filter)
	, initDir(directory)
{
	init(_model);
}

MyFileDialog::~MyFileDialog()
{

}

void MyFileDialog::init(int _model)
{
    this->setWindowTitle(tr("File Path Set Dialog"));
    this->setLabelText(QFileDialog::LookIn,tr("MyLookIn"));
    this->setLabelText(QFileDialog::FileName,tr("MyfileName"));
    this->setLabelText(QFileDialog::FileType,tr("MyFileType"));
    QString _appDir = QCoreApplication::applicationDirPath();
    switch(_model)
    {
    	case 0:
    	    this->setAcceptMode(QFileDialog::AcceptSave);
		    this->setLabelText(QFileDialog::Accept,tr("mySave"));
	        this->setDirectory(_appDir);
            this->setNameFilter(tr("csv Files (*.csv)"));
    		break;
    	case 1:
    	    this->setAcceptMode(QFileDialog::AcceptSave);
		    this->setLabelText(QFileDialog::Accept,tr("mySave"));
		    this->setDirectory(_appDir);
            this->setNameFilter(tr("xml Files (*.xml)"));
    		break;
    	case 2:
    	    this->setAcceptMode(QFileDialog::AcceptOpen);
		    this->setLabelText(QFileDialog::Accept,tr("myOpen"));
		    this->setDirectory(_appDir);
            this->setNameFilter(tr("xml Files (*.xml)"));
    		break;
    	case 7:
    	{
    		this->setAcceptMode(QFileDialog::AcceptOpen);
		    this->setLabelText(QFileDialog::Accept,tr("myOpen"));
	        this->setDirectory(_appDir);
            this->setNameFilter(tr("csv Files (*.csv);;"
		    	"ini files (*.ini);;"
		    	"Text files (*.txt);;"
		    	"xml files (*.xml);;"
		    	"Any files (*)"));
    	}
    		break;
    	case 8:
    	{
		    this->setAcceptMode(QFileDialog::AcceptOpen);
		    this->setLabelText(QFileDialog::Accept,tr("myOpen"));
            this->setNameFilter(tr("Any files (*)"));
		}
    		break;
    	default:
    		// this->setDirectory(_appDir);
    		break;
    }
    this->setLabelText(QFileDialog::Reject,tr("myCancel"));
}
