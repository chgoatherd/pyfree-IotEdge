#ifndef MYFILEDIALOG_H
#define MYFILEDIALOG_H

#include <QFileDialog>

class MyFileDialog : public QFileDialog
{
	Q_OBJECT
public:
	MyFileDialog( QWidget * parent = 0
		, int _model = 0
		, const QString & caption = QString()
		, const QString & directory = QString()
		, const QString & filter = QString());
	~MyFileDialog();
private:
	void init(int _model);
private:
	QString initDir;
	/* data */
};

#endif //MYFILEDIALOG_H
