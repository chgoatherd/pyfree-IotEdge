#include "mystatusbar.h"

MyStatusBar::MyStatusBar()
{
}

MyStatusBar::~MyStatusBar()
{

}

void MyStatusBar::init()
{
    setAutoFillBackground(true);
    setPalette(QColor(150,150,150));

    QFont _font("Courier New", 10);
    _font.setBold(true);
    this->setFont(_font);
}
