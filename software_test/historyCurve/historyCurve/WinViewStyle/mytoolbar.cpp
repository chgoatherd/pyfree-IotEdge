#include "mytoolbar.h"

MyToolBar::MyToolBar(QWidget *parent) :
    QToolBar(parent)
{
    init();
}

MyToolBar::MyToolBar( const QString & title, QWidget * parent ) : 
	QToolBar(title, parent)
{
	init();
}

MyToolBar::~MyToolBar()
{

}

void MyToolBar::init()
{
    this->setIconSize(QSize(16,16));
    setAutoFillBackground(true);
    setPalette(QColor(200,200,200));
}
