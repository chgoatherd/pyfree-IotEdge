
INCLUDEPATH              += $$ProDir/sqliteModel
INCLUDEPATH              += $$ProDir/SA_db

HEADERS += sqliteModel/sqlite3.h \
            sqliteModel/sqlite3ext.h \
            sqliteModel/CSQLite.h \
            SA_db/SADef.h \
            SA_db/SADB.h \

SOURCES += sqliteModel/sqlite3.c \
            sqliteModel/CSQLite.cpp \
            SA_db/SADB.cpp \
