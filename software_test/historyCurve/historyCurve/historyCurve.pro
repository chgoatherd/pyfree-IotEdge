# Add more folders to ship with the application, here
QT += xml
QT += network

TEMPLATE = app
DESTDIR = bin

CONFIG += qt warn_on debug_and_release
CONFIG += static

ProDir = $${PWD}
CONFIG(debug, debug|release) {
    win32{
    TARGET          = historyCurve_win_x64d
    OBJECTS_DIR     = debug_win/obj
    MOC_DIR         = debug_win/moc
    }
    unix{
    TARGET          = historyCurve_unix_x64d.exe
    OBJECTS_DIR     = debug_linux/obj
    MOC_DIR         = debug_linux/moc
    }
    CONFIG          += console
    DEFINES         += _DEBUG
} else {
    win32{
    TARGET          = historyCurve_win_x64
    OBJECTS_DIR     = release_win/obj
    MOC_DIR         = release_win/moc
    }
    unix{
    TARGET          = historyCurve_unix_x64.exe
    OBJECTS_DIR     = release_linux/obj
    MOC_DIR         = release_linux/moc
    }
    DEFINES         += NODEBUG
}

DEFINES += opengl

DEFINES += QT_CORE_LIB \
    QT_GUI_LIB \
    QT_THREAD_SUPPORT \
    QT_QWT_DLL

INCLUDEPATH              += $$ProDir
INCLUDEPATH              += $$ProDir/src

DEPENDPATH  += $$ProDir/src

HEADERS = src/mainwindow.h \
        src/CLoseCHeck.h \
        src/confdeal.h \

SOURCES = src/main.cpp \
    src/mainwindow.cpp \
    src/CLoseCHeck.cpp \
    src/confdeal.cpp \

HEADERS += WinViewStyle/mymenubar.h \
        WinViewStyle/mymenu.h \
        WinViewStyle/mytoolbar.h \
        WinViewStyle/mystatusbar.h \
        WinViewStyle/myIconbutton.h \
        WinViewStyle/myfiledialog.h \


SOURCES += WinViewStyle/mymenubar.cpp \
        WinViewStyle/mymenu.cpp \
        WinViewStyle/mytoolbar.cpp \
        WinViewStyle/mystatusbar.cpp \
        WinViewStyle/myIconbutton.cpp \
        WinViewStyle/myfiledialog.cpp \


#图表接口
include($$ProDir/plot.pri)
#sqlite
include($$ProDir/dbio.pri)
#sa
include($$ProDir/dlsa.pri)

unix{
    LIBS       += -ldl -lrt
}

#产品说明文件
RC_FILE += $$ProDir/app.rc
#翻译
TRANSLATIONS += $$ProDir/languages/historyCurve_cn.ts

RESOURCES += $$ProDir/historyCurve.qrc

