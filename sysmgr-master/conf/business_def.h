#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _BUSINESS_DEF_H_
#define _BUSINESS_DEF_H_
/***********************************************************************
  *Copyright 2020-04-12, pyfree
  *
  *File Name       : business_def.h
  *File Mark       : 
  *Summary         : 程序数据缓存,单体类
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <map>

#include "conf.h"
#include "datadef.h"

class BusinessDef
{
public:
	static BusinessDef* getInstance();
	static void Destroy();
	~BusinessDef();
public:
	//aliyun IOT config
	inline bool getAliyunFunc()
	{
		return appConf.aliyunfunc;
	};
	pyfree::AliyunTriples getGateway()
	{
		return appConf.gateway;
	};
	//ota config
	pyfree::OTAAliyun getOTAAliyun()
	{
		return appConf.otaInfo;
	};
	//web config
	inline bool getwebfunc()
	{
		return appConf.webfunc;
	};
	inline std::string getWebIp()
	{
		return appConf.webconf.ip;
	};
	inline int getWebPort()
	{
		return appConf.webconf.port;
	}
	//logdel info
	inline std::string getGLogDir()
	{
		return appConf.saconf.gLogDir;
	};
	inline char getDiskSymbol()
	{
		return appConf.saconf.diskSymbol;
	};
	inline int getFreeSizeLimit()
	{
		return appConf.saconf.freeSizeLimit;
	};
	inline int getDayForLimit()
	{
		return appConf.saconf.dayForLimit;
	};
public:
	/**
	 * 获取被管理服务描述信息,从服务配置信息读取转换
     * @param svcTriples {map& } 服务描述信息,还该服务配置以及服务态势等信息
	 * @return {bool}
	 */
	bool getSvcInfo(std::map<int, SvcDesc> &svcTriples);
	/**
	 * 获取被管理的服务配置信息
	 * @return {map} 服务配置信息
	 */
	std::map<int, pyfree::ServiceInfo> get_svcmaps();
private:
	BusinessDef();
	BusinessDef& operator=(const BusinessDef&) { return *this; };
	void init();
private:
	static BusinessDef* instance;
	//app运行参数配置集
	pyfree::ServiceConf appConf;
	std::map<int, pyfree::ServiceInfo> svcmaps;
};
#endif
