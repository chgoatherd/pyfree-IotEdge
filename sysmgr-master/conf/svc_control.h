#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SVC_CONTROL_H_
#define _SVC_CONTROL_H_

/***********************************************************************
  *Copyright 2020-04-16, pyfree
  *
  *File Name       : svc_control.h
  *File Mark       : 
  *Summary         : 服务控制函数集合,依赖../svc_common的实现
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#include <string>

/*
*在真正实现svc_common目录下,win见WinSVC.cpp,而linux见linuxSVC.cpp
*/
void SvcStart(char *svr);
void SvcStop(char *svr);
void SvcQuery(char* svr, int &svc_state);

namespace pyfree
{
    /**
     * 根据服务名控制服务启停
     * @param svc_name {string } 服务名
     * @param cmd {int } 执行动作,1 停止, 2 运行, 3 暂停(待实现)
     * @return {void}
     */
    void svc_control(std::string svc_name, int cmd);
    /**
     * 根据服务名获取服务状态描述
     * @param svc_name {string } 服务名
     * @return {string} 服务状态描述
     */
    std::string get_svc_status_desc(char* svc_name);
};

#endif
