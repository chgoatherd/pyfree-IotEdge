#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _COMSUMER_MQTT_ALIYUN_H_
#define _COMSUMER_MQTT_ALIYUN_H_
/***********************************************************************
  *Copyright 2020-04-12, pyfree
  *
  *File Name       : ConsumerMqttAliyun.h
  *File Mark       : 
  *Summary         : 阿里云订购线程
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"
#include <map>
#include "datadef.h"
#include "queuedata_single.h"

class ConsumerMqttAliyun : public acl::thread
{
public:
	ConsumerMqttAliyun(std::map<std::string, AliyunServiceDesc> subTopicMaps_);
	virtual ~ConsumerMqttAliyun();
	//
	void* run();
private:
	ConsumerMqttAliyun(const ConsumerMqttAliyun&);
	ConsumerMqttAliyun& operator=(const ConsumerMqttAliyun&) { return *this; };
private:
  /**
	 * 从来自阿里云物联网平台下行数据的缓存数据队列提取数据进行业务处理,由阿里接口的回调函数实现数据接收和写入队列
   * @param buf {acl::string& } 返回缓存
	 * @return {void}
	 */
	void receive();
private:
	bool running;
	std::map<std::string, AliyunServiceDesc> subTopicMaps;
	QueueDataSingle<RCacheAliyun> *rec_queue; //待下发采集端的缓存数据
};


#endif
