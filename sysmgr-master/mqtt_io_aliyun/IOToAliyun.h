#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _IO_TO_ALIYUN_H_
#define _IO_TO_ALIYUN_H_
/***********************************************************************
  *Copyright 2020-04-12, pyfree
  *
  *File Name       : IOToAliyun.h
  *File Mark       : 
  *Summary         : 阿里云物联网平台接口
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include <string>
#include <map>

#include "conf.h"
#include "datadef.h"
#include "queuedata_single.h"

class ProducerMqttAliyun;
class ConsumerMqttAliyun;

class IOToMqttAliyun : public acl::thread
{
public:
	IOToMqttAliyun(void);
	virtual ~IOToMqttAliyun(void);

	void* run();
private:
	void init();
	void init_config();
	void init_sub_top();
	void init_iot();
	void init_ota();
	void construct_iot();
	void uninit();
	/**
	 * 构建与平台通信的接口
	 * @return {void}
	 */
	void create();
	/**
	 * 释放与平台通信的接口
	 * @return {void}
	 */
	void destroy();
	/**
	 * 消息订购
	 * @return {void}
	 */
	void subscribe();
	/**
	 * 取消消息订购
	 * @return {void}
	 */
	void unsubscribe();
	void send();
	/**
	 * 固件升级,从平台下载升级包
	 * @return {void}
	 */
	void ota_down();
	/**
	 * 获取固件版本信息
	 * @param version_def {char*} 固件描述信息
	 * @return {void}
	 */
	int getVersion(char* version_def);
	/**
	 * 设置固件版本信息
	 * @param version_def {char*} 固件描述信息
	 * @return {void}
	 */
	void setVersion(char* version_def);
	/**
	 * 根据升级包,升级被监控服务
	 * @return {void}
	 */
	void update();
private:
	bool running;			//线程运行标记
	bool initLink;			//接口第一次链接标记
	pyfree::AliyunTriples gatewayTriples;	//三元组信息
	std::map<int, SvcDesc> svcTriples;		//服务描述信息
	std::map<std::string, AliyunServiceDesc> subTopicMaps;	//根据服务信息构建的主题信息
	void *pclient;		//阿里云物联网平台通信接口句柄
	void *h_ota;		//阿里云物联网平台OTA接口句柄
	OTAAliyunCache otaInfo;
	std::string curDir;
	char *msg_buf;
	char *msg_readbuf;
	unsigned long long cnt;
	ProducerMqttAliyun *producer;		//生产者线程
	ConsumerMqttAliyun *consumer;		//消费者线程
	QueueDataSingle<WCacheAliyun> *send_queue; //待下发采集端的缓存数据
	// QueueDataSingle<RCacheAliyun> *rec_queue; //待下发采集端的缓存数据
};

#endif
