#ifndef _CONF_READ_H_
#define _CONF_READ_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_read.h
  *File Mark       : 
  *Summary         : 业务数据读取函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <map>
#include <string>

#include "conf_app.h"

namespace pyfree
{
// bool isExist(std::string _xml = "appconf.xml");
/**
 * 从指定xml文件中读取程序的配置信息
 * @param conf {AppConf&} 程序配置信息
 * @param xml_ {string} xml文件名
 * @retuan {void} 
 */
void readAppConf(AppConf &conf, std::string xml_ = "appconf.xml");
/**
 * 向指定xml文件中写入程序的配置信息
 * @param conf {AppConf&} 程序配置信息
 * @param xml_ {string} xml文件名
 * @retuan {void} 
 */
void writeAppConf(const AppConf conf, std::string xml_ = "appconf.xml");
};
#endif //CONF_READ_H